# All available Hugo versions are listed here: https://gitlab.com/pages/hugo/container_registry
image: registry.gitlab.com/pages/hugo:latest

variables:
    GIT_SUBMODULE_STRATEGY: recursive
    REVIEW_APP_URL: "https://$CI_PROJECT_NAMESPACE.gitlab.io/-/$CI_PROJECT_NAME/-/jobs/$CI_JOB_ID/artifacts/public/"

stages:
    - Check
    - Test
    - Review
    - Performance
    - Deploy

# ----------------------------------------------------------------------------------------------------------------------
include:
    - project: 'ordinarynick-portfolio/infrastructure/common-ci-pipelines'
      ref: v0.3
      file: '/gitlab-ci_lint.yaml'

# ----------------------------------------------------------------------------------------------------------------------
# Tests if repository is ok and ready to generate files.
Test:
    stage: Test
    script: hugo
    rules:
        - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'

# Publish generated files by Hugo to Gitlab pages. Unfortunately job must be named "pages" to be able to deploy.
pages:
    stage: Deploy
    environment:
        name: prod/Gitlab-pages
        url: https://ordinarynick-portfolio.gitlab.io/ordinarynick.com/
    script: hugo --minify
    artifacts:
        paths:
            - public
    rules:
        - if: '$CI_COMMIT_BRANCH == "master"'

# Publish generated files by Hugo as Gitlab review app.
Pages:Review:
    stage: Review
    variables:
        REVIEW_APP_URL: "https://$CI_PROJECT_NAMESPACE.gitlab.io/-/$CI_PROJECT_NAME/-/jobs/$CI_JOB_ID/artifacts/public/"
    environment:
        name: review/$CI_COMMIT_REF_NAME
        url: https://$CI_PROJECT_NAMESPACE.gitlab.io/-/$CI_PROJECT_NAME/-/jobs/$CI_JOB_ID/artifacts/public/index.html
    script:
        - sed -i "s,https://ordinarynick-portfolio.gitlab.io/ordinarynick.com/,$REVIEW_APP_URL,g" config.toml
        - hugo
    artifacts:
        paths:
            - public
    rules:
        - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'

# Measure performance of web with sla.
Pages:Performance:
    stage: Performance
    image: registry.gitlab.com/ordinarynick-portfolio/common-docker-images/lighthouse
    variables:
        INDEX_URL: /index.html
        POSTS_URL: /post/index.html
    before_script:
        - sed -i "s,INDEX_URL,$INDEX_URL,g" lighthouserc.json
        - sed -i "s,POSTS_URL,$POSTS_URL,g" lighthouserc.json
    script:
        - lhci autorun || echo "LHCI failed!"
    artifacts:
        paths:
            - .lighthouseci/
    dependencies:
        - Pages:Review
    rules:
        - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
