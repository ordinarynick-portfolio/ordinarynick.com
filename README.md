[![pipeline status](https://gitlab.com/ordinarynick-portfolio/ordinarynick.com/badges/master/pipeline.svg)](https://gitlab.com/ordinarynick-portfolio/ordinarynick.com/-/commits/master)

---
Sources of Ordinary Nick's webpage. Base of this repository was created from Gitlab Hugo template.

---

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [Where blog is published](#whre-blog-is-published)
- [GitLab CI](#gitlab-ci)
- [Building locally](#building-locally)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Where is website published?
Website is published on two domains. First is gitlab pages domain https://ordinarynick-portfolio.gitlab.io/ordinarynick.com/ and second is on my own domain https://ordinarynick.com.

## GitLab CI

This project's static Pages are built by [GitLab CI][ci], following the steps
defined in [`.gitlab-ci.yml`](.gitlab-ci.yml).

## Building locally

To work locally with this project, you'll have to follow the steps below:

1. Fork, clone or download this project
1. [Install](https://gohugo.io/getting-started/installing/) Hugo
1. Preview your project: `hugo server`
1. Add content
1. Generate the website: `hugo` (optional)

Read more at Hugo's [documentation](https://gohugo.io/getting-started/).

### Preview your site

If you clone or download this project to your local computer and run `hugo server`,
your site can be accessed under `localhost:1313/hugo/`.

The theme used is adapted from http://themes.gohugo.io/beautifulhugo/.
